"""
Python 3.6+

python sample.py


Keyword Patterns (already regular expressions) --
['los\\w+\\W*interest',
 'grandiose\\W*(thought|think)',
 'racing\\W*(thought|think)',
 'intrusive\\W*(thought|think)',
 'impulsiv\\w+',
 'distracted',
 '(unable|can(no|\\W)t|inability)\\W*(to\\W*)?sit\\W*still',
 'not\\W*listen\\w*',
 'anxious',
 'anxiety',
 '\\bsad\\b',
 'depressed',
 'disinterest',
 'fighting',
 'suicidal',
 'oppositional',
 'inattentive',
 '\\bADD\\b',
 '\\bADHD\\b',
 'depression',
 'persever(ation|ing)',
 'panic',
 '(poor|low|fail\\w*)\\W*grades',
 '(poor|low|fail\\w*)\\W*(at\\W*)?school',
 'withdraw(n|ing)',
 'sensory\\W*aversion',
 'attack\\w*\\W*((his|her)\\W*)?mom',
 'hoard\\w*\\W*food',
 'refus\\w*\\W*to\\W*eat',
 'dark\\W*(think|thought)',
 'repet\\w+\\W*behavior',
 'heart\\W*racing',
 'fear(ful)?\\W*of',
 'delusional',
 'paranoid',
 'mood\\W*swing',
 'school\\W*trouble',
 '(unable|can(no|\\W)t|inability)\\W*(to\\W*)?sleep',
 '(difficulty|trouble|tough\\W*to)\\W*(getting\\W*to\\W*)?sleeping',
 '(sleeping\\W*(excessively|too\\W*much)|excessive\\W*sleeping)',
 'harm\\w*\\W*self',
 '(stay\\w*|always)\\W*awake',
 'obsessive',
 'compulsive',
 '\\bOCD\\b',
 '\\btics\\b',
 'hallucinat\\w+',
 'fantas(y|ies)',
 'nonsensical',
 'bull(ying|ies)',
 'divorce',
 '(arriv|mov)\\w+\\W*from',
 '(arriv|mov)\\w+\\W*(to|in)\\W*Seattle',
 'new\\W*school',
 'no\\W*friend',
 '(no|minimal|lack\\W*of)\\W*eye\\W*contact',
 'not\\W*speak',
 'bing\\w+',
 'purg\\w+',
 'manic',
 'mania',
 'autis\\w+',
 '\\bASD\\b',
 'irritab\\w+',
 'mood\\W*lability',
 'substance\\W*use',
 'hyperactivity',
 '\\bPTSD\\b',
 'borderline',
 'anorexia',
 'bul[ei]mia',
 'disassociation',
 'flashbacks?',
 'nightmares?',
 'aggressive',
 'dysregulated',
 'disruptive']
"""
import datetime
import re


def get_patterns():
    """List of keywords"""
    return [re.compile(re.sub(' +', r'\W*', k.strip()), re.I)
            for k in  r'''los\w+ interest
    grandiose (thought|think)
    racing (thought|think)
    intrusive (thought|think)
    impulsiv\w+
    distracted
    (unable|can(no|\W)t|inability) (to )?sit still
    not listen\w*
    anxious
    anxiety
    \bsad\b
    depressed
    disinterest
    fighting
    suicidal
    oppositional
    inattentive
    \bADD\b
    \bADHD\b
    depression
    persever(ation|ing)
    panic
    (poor|low|fail\w*) grades
    (poor|low|fail\w*) (at )?school
    withdraw(n|ing)
    sensory aversion
    attack\w* ((his|her) )?mom
    hoard\w* food
    refus\w* to eat
    dark (think|thought)
    repet\w+ behavior
    heart racing
    fear(ful)? of
    delusional
    paranoid
    mood swing
    school trouble
    (unable|can(no|\W)t|inability) (to )?sleep
    (difficulty|trouble|tough to) (getting to )?sleeping
    (sleeping (excessively|too much)|excessive sleeping)
    harm\w* self
    (stay\w*|always) awake
    obsessive
    compulsive
    \bOCD\b
    \btics\b
    hallucinat\w+
    fantas(y|ies)
    nonsensical
    bull(ying|ies)
    divorce
    (arriv|mov)\w+ from
    (arriv|mov)\w+ (to|in) Seattle
    new school
    no friend
    (no|minimal|lack of) eye contact
    not speak
    bing\w+
    purg\w+
    manic
    mania
    autis\w+
    \bASD\b
    irritab\w+
    mood lability
    substance use
    hyperactivity
    \bPTSD\b
    borderline
    anorexia
    bul[ei]mia
    disassociation
    flashbacks?
    nightmares?
    aggressive
    dysregulated
    disruptive'''.strip().split('\n')]


def select_notes(records, cutoff=100, min_length=100,
                 pattern_reset_num=10):
    """
    Select `cutoff` notes containing certain keywords.
        Prefers unseen keywords until the number of 'unseen' keywords
        is less than `pattern_reset_num`.

    :param pattern_reset_num: when this many unseen patterns remain, add back all patterns
    :param min_length: minimum length of notes to include
    :param cutoff: number of records to return
    :param records: list/iterator of (note_id, note_text)
    :return:
    """
    keywords = get_patterns()
    curr_keywords = list(keywords)  # copy for removing
    chosen = set()
    for note_id, text in records:
        if len(text.strip()) < min_length:
            continue  # don't include short notes
        found = None
        # look for each keyword that hasn't yet been found
        for i, keyword in enumerate(curr_keywords):
            if keyword.search(text):
                found = i
                break
        if found is not None:  # we found something
            curr_keywords.pop(found)  # remove found keyword
            chosen.add(note_id)  # add this as a note to look at
            if len(curr_keywords) < pattern_reset_num:
                # add more keywords if we're running low
                #  as some keywords may not be found
                curr_keywords += list(set(keywords) - set(curr_keywords))
        if len(chosen) >= cutoff:
            break  # found all we need
    return chosen


def write_to_file(it, name):
    dt = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    with open(f'{name}_{dt}.txt', 'w') as out:
        out.write('\n'.join(str(i) for i in it))


if __name__ == '__main__':
    # dump patterns to a file
    write_to_file((k.pattern for k in get_patterns()), 'patterns')
    # example notes
    records = [
        ('21', 'acting aggressively'),  # yes
        ('205', 'acting aggressively again'),  # no, repeat
        ('240', 'nightmares and flashbacks'),  # yes, 'nightmare'
        ('241', 'nightmares and flashbacks'),  # yes, 'flashback'
        ('408', 'nothing here'),  # no
    ]
    selected_ids = select_notes(records, min_length=4)
    assert len(selected_ids) == 3
    # dump found ids to file
    write_to_file(selected_ids, 'selected_ids')
